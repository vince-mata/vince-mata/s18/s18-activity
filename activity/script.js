/*
1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function
*/

let myPokemon = {
	name: 'Charmander',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log("This pokemon tackled targetPokemon")
	},
	faint: function() {
		console.log("targetPokemon fainted")
	}
}

console.log(myPokemon);

//We will use object constructor


function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	//methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled '+ target.name);
		target.health=target.health - this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);
			if(target.health <=5){
				target.faint()
			}
	
	},
	this.faint = function() {
		console.log(this.name + "fainted.");
	}

}

let charmander = new Pokemon("Charmander", 16);
let bulbasaur = new Pokemon("Bulbasaur", 8);

charmander.tackle(bulbasaur)
charmander.tackle(bulbasaur)
